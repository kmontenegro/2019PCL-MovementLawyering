## Detailed Schedule  
Please have the reading assigment completed before you get to school. For this class, in spite of being a law school class, the reading is intentionally accessible. Please respect the time of your fellow students/attendees and come prepared.  

The Week Two class will be via video platform Zoom (make sure you've tested it before you attend the class). There will be no in-person class during Week Five: students are expected to watch both films and email a three to five paragraph critique/set of observations. Week Nine will be via Zoom and will be a short class (e.g. likely one and a half hours).

Guest dates might slide and more guests will be added course pace permitting.

### Note for auditing attendees  
It's wonderful that you're interested in auditing this course. You are expected to have completed the reading if you choose to join us in class. I will focus on the students who are taking this class for credit. I also reserve the right to ask you to leave if you have not read the material and your well-intentioned attempts to participate are disruptive. We all have to be on the same level for the discusssions to accomplish their intended learning objectives.  

## Week One [3/6]  
* Normal Life: Chapters 1 and 2  
* Betty Hung: Law and Organizing  
* Victor Narro: Letter to a Young Public Interest Attorney  
## Week Two [3/13] (Ken in Portland)  
* Normal Life: Chapters 5 and Conclusion
* Miriam Kaba: It's Not Civil Disobedience If You Ask For Permission  
* Samuel Moyn: Law Schools Are Bad for Democracy  
* [Haywood Burns on the US Judicial System](https://youtu.be/dPV29_KHzQM)  
* [Chomsky & Foucault Justice vs Power](https://youtu.be/J5wuB_p63YM)  
* [Open Mind: Conversation with Bill Kunstler](https://youtu.be/0kkHnTRz1zA)  
## Week Three [3/20]  
* Haywood Burns, Racism & American Law (Law Against the People)  
* Crystal Flemming, Why Are We Talking About White Supremacy  
* William Kunstler, Open Resistance: In Defense of the Movement  
## Week Four [3/27]  
* Paper topic approved
* Dean Spade, review of [The Law Is A White Dog](http://www.deanspade.net/wp-content/uploads/2013/08/dayanspadejle.pdf)  
* Florynce Kennedy, The Whorehouse Theory of Law (Law Against the People)  
~~* S Lamble: Transforming Carceral Logics~~  
* Michael J Kennedy, The Civil Liberties Lie (Law Against the People)
## Week Five [4/3] (Ken In Spain)  
* Movies: Albie Sachs & William Kunstler  
## Week Six [4/10]  
* Arthur Kinoy, The Radical Lawyer and Teacher of Law (Law Against the People)
* Poor People's Movements: Introduction, Chapters 1 and 2  
* Guests (TBD)  
## Week Seven [4/17]  
* No More Heroes: Introduction, Chapters 1 and 11  
* Guests (TBD)  
## Week Eight [4/24]  
* Major paper due
* No More Heroes: Chapters 8, 9, 10  
## Week Nine [5/1] (Ken in NYC)  
* Major paper discussion  
* Poor Peoples Movements: Chapter 5  
* Guests (TBD)
## Week Ten [5/8]  
* Evaluation
* How Nonviolence Protects the State: Chapters 1, 2, & 3  
* Dylan Rodriguez: The Political Logic of the Nonprofit Industrial Complex  
* Miriam Kaba: Police Reforms You Should Always Oppose  
