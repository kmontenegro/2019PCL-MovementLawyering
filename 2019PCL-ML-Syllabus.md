# Movement Lawyering (PCL Spring 2019)
### Dates (scheduled but subject to change)  
Wednesday evenings (6:30pm) between March 5 - May 15, 2019   
No class on April 3 (students are expected to self-study)

#### Instructor  
[Ken Montenegro](https://www.comeuppance.net/about-ken/)  

#### Virtual Office Hours (Zoom or phone available)  
Wednesday 5-6 pm  
Thursday 1-3:15 pm

## Course Overview  
This course will critically demystify the concept of "movement lawyering" and learn models of interaction/support between lawyers and social movements.  

Historically, lawyers have been perceived as being outside revolutionary struggle unless they are engaged in traditional practice of law (e.g. litigation, policy work/reform etc). This course will encourage upper division law students to explore their politics; seek to define “movement lawyering”; and interrogate what traits or roles lawyers can have within social movements. This course will also explore the phraseology of “social justice” and how that might inform our understanding of “movement lawyering” and movements themselves.  

This course seeks to understand how the law is not always an ally to social movements and, sometimes, an impediment to radical social transformation.  Students will be expected to reflect upon and bring their previous schoolwork and whole-life experiences to this subject matter.

## Required Texts  
You are encouraged to buy used books and to share these books with your comrades at the conclusion of this course.
* [No More Heroes](https://www.akpress.org/nomoreheroes.html), Jordan Flaherty  
* [Normal Life](http://www.deanspade.net/books/normal-life/), Dean Spade  
* [Poor Peoples Movements](https://www.amazon.com/Poor-Peoples-Movements-They-Succeed/dp/0394726979), Piven and Cloward  
* [Law Against the People](https://www.amazon.com/Law-Against-People-Demystify-Courts/dp/039471038X/ref=sr_1_1?s=books&ie=UTF8&qid=1535408779&sr=1-1&keywords=law+against+the+people&dpID=41Q%252BuNHET8L&preST=_SY291_BO1,204,203,200_QL40_&dpSrc=srch), Moore

## Supplemental Materials  
* Course reader provided by instructor (duplication fees to be determined)  
* Internet access for a remote participation class
* Laptop computer
* Webcamera  
* Headphone & microphone  
* A Los Angeles City or County Public Library Card (to access the films)

## Course Schedule (subject to modification)  
### Week Number and Reading  
*Accurate [dates and assignments can be found here](https://gitlab.com/kmontenegro/2019PCL-MovementLawyering/blob/master/2019PCL-ML-DetailedSchedule.md)*  
~~* Wk0   Movies (through Kanopy): "Soft Vengance: Albie Sachs and the New South Africa" and "William Kunstler: Disturbing the Universe"~~  
~~* Wk1	Normal Life: Introduction, Chapters 1 and 2~~  
~~* Wk2	Normal Life: Chapter 5 and Conclusion~~  
~~* Wk3	Discussion (political awakening & change visions)~~  
~~* Wk4	Discussion (inventory and analysis of movement lawyers)~~  
~~* Wk5	Poor People's Movements: Introduction, Chapters 1 andd 5~~  
~~* Wk6	Discussion (Quiqly & Lefebre readings)~~  
~~* Wk7	No More Heroes: Introduction, Chapters 1 and 11~~
~~* Wk8	Discussion (Guests)~~  
~~* Wk9	No More Heroes: Chapters 8, 9, 10, & 11~~  
~~* Wk10	Discussion (visions of change) & evaluation~~  

### Major Assignment  
Students will be responsible for a paper between 7 and 10 pages. Paper topics are to be approved by the instructor.  Each student should have their paper topic approved no later than week 4; papers are due at least 48 hours before the class of Wk8.  

All work produced throughout the course will be Creative Commons Licensed.  

## Grading Rubric  
1 50% major paper  
2 10% attendance (excused absences do not count against students)  
3 10% personal statement & film assessment  
4 10% in-class participation  
5 20% evaluation


## Additional Information  
Attendance to this class is crucial for understanding the material. 
 

