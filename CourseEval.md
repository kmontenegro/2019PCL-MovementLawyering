This is a one-hour in-class writing assignment. 

## Evaluation/Reflection  
* Which reading impacted you the most and why?  
* As you prepare to complete law school, how do you believe this course shifted (or didn't shift) your understanding of the law in social change movements?  
* What legal concept, theory, or precept did this course radically alter, shift, or change?  
* Where do you believe critical thought fits/doesn't fit in a legal education?  
* What's one thing you would change about the course?  