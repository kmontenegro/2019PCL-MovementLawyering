* Sharlyn Grace, Chicago Bond Fund  
* Pooja Gehi, NLG  
* Natasha Banan, Latino Justice  
* Mia Yamamoto    
* Nathan Sheard, Standing Rock & Ferguson Legal Support  
* Hamid Khan & Pete White  

---
## Potential Questions  
1 What is movement lawyering to you?  
2 How did you get to this definition?  
3 Going back 10 years, what's something you'd tell a young lawyer coming into your movement space/work?  
4 In your work, can you share an anecdote of where you saw movement lawyering deliver goods that might not have been possible with a traditional legal approach?  
5 How do you stay renewed and connected in this work?  