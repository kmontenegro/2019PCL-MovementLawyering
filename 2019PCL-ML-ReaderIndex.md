## Reader Index  
1 Balagoon, Kuwasi. A Soldier's Story: Writings by A Revolutionary New Afrikan Anarchist. Opening statement & closing statement.  
2 Rodriguez, Dylan. The Revolution Will Not Be Funded. "The Political Logic of the Nonprofit Industrial Complex".    
3 Gelderloos, Peter. How Nonviolence Protects the State. Chapters 1,2, & 3  
4 Meiners, Erica R. Captive Genders eds. Stanley & Smith. "Aweful Acts & the Trouble with Normal"  
5 Lamble, S. Captive Genders eds Stanley & Smith. "Transforming Cerceral Logics"  
6 Kaba, Miriam. [Police Reforms You Should Always Oppose](http://www.usprisonculture.com/blog/2014/12/01/police-reforms-you-should-always-oppose/).    
7 Kaba, Miriam et al. [It's Not Civil Disobedience If You Ask For Permission](https://truthout.org/articles/its-not-civil-disobedience-if-you-ask-for-permission/).   
8 Kennedy, Duncan. TBD  
9 Spade, Dean. [Notes towards Racial and Gender Justice Ally Practice in Legal Academia](http://www.deanspade.net/wp-content/uploads/2010/07/Spade-presumed-incompetent-contribution-final.pdf). http://www.deanspade.net/wp-content/uploads/2010/07/Spade-presumed-incompetent-contribution-final.pdf  
10 Haney-Lopez, Ian. TBD  
11 Spade, Dean. [Book Review of "The Law Is A White Dog: How Legal Rituals Make and Unmake Persons"](http://www.deanspade.net/wp-content/uploads/2013/08/dayanspadejle.pdf).  
12 Burns, Haywood. Racism and American Law.  
13 Kennedy, Florynce. The Whorehouse Theory of Law  
14 Quigley, Bill. TBD  
15 Narro, Victor and Hung, Betty. TBD  
16 Moyn, Samuel. [Law Schools Are Bad for Democracy](https://www.chronicle.com/article/Law-Schools-Are-Bad-for/245334?key=1brOtA2hhki0d3uawry_7keK-LKkePsVzDJMg51ZD2KJK4l8zeJVeGfM04AyDnaHQkFYRWdhclZTWWRvZFZEVVZKLWhNYXMtbzR1TFlrdzNiRGNpblhiR001SQ) (in the Chronicle of Higher Education)  
17 Crystal M Flemming, [Why Are We Talking About White Supremacy?](https://www.uuworld.org/articles/idiots-guide-critical-race-theory)